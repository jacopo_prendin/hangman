
# -*- coding: utf-8 -*-
import json
import os
import datetime as dt

class HangmanScore:
    def __init__(self,player,score,date = None):
        self.player = player
        self.score = score
        if date:
            self.date = date
        else:
            self.date = dt.datetime.now()
            
    def __eq__(self,other):
        self.score == other.score

    def __lt__(self,other):
        return self.score < other.score

    
class TxtScoreBoard:
    def __init__(self,filepath):
        '''
        Loads the score objects from a txt file and sort them. A better 
        solution would be a SQLite file, but for a proof-of-concept should
        be enough
        '''
        self.scores = []
        self.full_filepath = filepath

        # scan the whole file (if it exists) and parse each line. An entry is
        # a line with tab as separator. I know I could use a JSON, but this is
        # done just as prototype
        if os.path.exists(self.full_filepath):
            with open(self.full_filepath) as scorefile:
                for entry in scorefile.readlines():
                    fields = entry[:-1].split("\t")
                    self.scores.append( HangmanScore(
                        fields[0],
                        int(fields[1]),
                        dt.datetime.strptime(fields[2],"%Y-%m-%dT%H:%M"))
                        )
            self.scores.sort(reverse=True)

    def save(self, player,score):
        '''
        Simply dumps the player informations into a txt file
        '''
        newentry = HangmanScore(player,score)
        self.scores.append(newentry)
        self.scores.sort(reverse=True)
        with open(self.full_filepath, 'w') as scorefile:
            for entry in self.scores:
                scorefile.write('{0}\t{1}\t{2}\n'.format(
                    entry.player,
                    entry.score,
                    entry.date.strftime("%Y-%m-%dT%H:%M")
                ))

    def top_ten(self):
        '''
        Returns the highest ten scores. If they're less than 10 scores,
        we simply get'em all
        '''
        try:
            return self.scores[0:10]
        except Exception:
            return self.scores

# ------------------------------------------------------------------------------
# 
#                              SIMPLE TEST
# 
# ------------------------------------------------------------------------------
if __name__=='__main__':
    sb = TxtScoreBoard('./testboard.scr')
    for p in sb.top_ten():
        print("{0}\t{1}".format(p.player, p.score))

    
