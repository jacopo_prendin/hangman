

# -*- coding: utf-8 -*-
from hangman import Hangman
from txtscoreboard import TxtScoreBoard

words = ['3dhubs', 'marvin', 'print', 'filament', 'order', 'layer']

scoreboard = TxtScoreBoard('./scoreboard.txt')
play = True
reply = 'x'

while play:
    # at every play, we create a new Hangman object, with a new player
    print(words)
    hangman = Hangman(words)
    hangman.player_name = raw_input("What's your name? ")
    while hangman.playing():
        print( "\n\n=== {0}: Match n. {1} ===".format(
            hangman.player_name,
            hangman.match) )
        print(hangman.current_word_status())
        print("Unmatching letters: {0}".format(','.join(hangman.unused_chars)))
        print("Attempts left: {0}".format(hangman.attempts_left()))

        # we can only ask for a single character not yet used
        character = ''
        while  character in hangman.used_chars \
               or character == '' or \
               len(character) != 1:
            character = raw_input("Choose a letter (or 1 to give solution): ")

        # working from console, we need a character to allow the user
        # to give the solution
        if character == '1':
            solution = raw_input("Your solution: ")
            if hangman.solution(solution):
                print("    !!! Good !!!")
            else:
                print("        :-( Solution is not {0}".format(solution))

        else:
            hangman.use(character)

    # check why we exit the hangman's playing loop
    if hangman.winner():
        print("Congratulatins!")
    else:
        print("Game Over")

    # save the score
    scoreboard.save(hangman.player_name, hangman.score)

    # continue or end the game?
    reply = 'x'
    while reply not in ('y','n'):
        reply = raw_input("Continue (y/n)? ")
        play = reply == 'y'

# at exit, print the topten
for tt in scoreboard.top_ten():
    print('{0}\t{1}'.format(tt.player, tt.score))
