
# -*- coding: utf-8 -*-

import time
import random

class Hangman:
    def __init__(self,words, max_attempts=10):
        self.avail_words=words[:] # compatible python 2.7/python3
        self.player_name = 'undefined'
        self.match = 0
        random.seed(time.time())

        self.current_word = ''
        self.used_chars = []
        self.unused_chars = []
        self.max_attempts = max_attempts
        self.attempts = 0
        self.score = 0
        self.new_match()

    def new_match(self):
        '''
        This procedure re-initialize a match, clearing the game state
        '''
        num_words = len(self.avail_words)
        if num_words > 0:
            self.current_word = self.avail_words[random.randint(0,num_words-1)]
            self.attempts = 0
            self.match += 1
            self.used_chars = []
            self.unused_chars = []
            
    def playing(self):
        '''
        We are still playing if we have still some attempts and we have
        words available
        '''
        return self.attempts < self.max_attempts and \
            len(self.avail_words)>0
            
    def current_word_status(self):
        '''
        Return a string with current status of matched letters
        '''
        status = []
        for c in self.current_word:
            if c in self.used_chars:
                status.append(c)
            else:
                status.append('_')
        return ''.join(status)

    def attempts_left(self):
        '''
        Used to inform the player of how many attempts he can still do
        '''
        return self.max_attempts - self.attempts

    def use(self,character):
        '''
        The attempt of guessing a character. The character is inserted in the
        according list and the attempts counter is incremented
        '''
        self.attempts += 1
        if character in self.current_word:
            self.used_chars.append(character)
        else:
            self.unused_chars.append(character)

    def solution(self,given_solution):
        '''
        An attempt to give the solution. If it's correct, increase the score
        and starts the new match; othewise it is considered as a failing attempt.
        Score in increased according to the number of attempts and the word's
        length
        '''
        if given_solution == self.current_word:
            self.score += self.max_attempts - self.attempts
            self.score += len(self.current_word)
            self.avail_words.remove(self.current_word)
            self.new_match()
            return True
        else:
            self.attempts += 1
            return False

    def winner(self):
        return len(self.avail_words) == 0
